'''
The objective of this exercise is to maintain a list of Strings in memory that support undo and redo. Write a program
that allows the user to add, edit, delete, undo, and redo entries in a list. You must be able to undo and redo
everything you've done during the execution of this program. After each command is run, always print out the list
(unless you're doing this in a UI). Before writing any code, first think about how to write add, edit, and remove
with undo and redo in mind.
'''


class EditText:
    def __init__(self):
        self.list = []
        self.dict_steps = {}
        self.dict_redo = {}

        self.ask_for_command()

    def ask_for_command(self):
        if len(self.list) == 0:
            print("<EMPTY LIST>\n")
        else:
            self.print_list()

        command = str(input("Enter command ('A'dd, 'E'dit, 'D'elete, 'U'ndo, 'R'edo): ")).lower()

        if command in ('a', 'add'):
            self.add()
        elif command in ('e', 'edit'):
            self.edit()
        elif command in ('d', 'delete'):
            self.delete()
        elif command in ('u', 'undo'):
            self.undo()
        elif command in ('r', 'redo'):
            self.redo()
        else:
            print("Wrong command\n")
            self.ask_for_command()

    # Add user input to the list. Add additional properties to self.dict_steps to make redo and undo methods
    # easier to use
    def add(self):
        string = str(input("Enter text to add: "))
        self.list.append(string)
        delete_index = len(self.list) - 1
        dict_size = str(len(self.dict_steps))

        self.dict_steps[dict_size] = ["add", delete_index, string] # type, what index to remove, what to remove

        self.ask_for_command()

    # Edit a given index to user's input. Add additional properties to self.dict_steps to make redo and undo methods
    # easier to use
    def edit(self):
        index, new_string = int(input("Enter index to edit: ")), str(input("Enter text to edit: "))
        old_string = self.list[index]
        dict_size = str(len(self.dict_steps))

        self.dict_steps[dict_size] = ["edit", index, old_string, new_string] # type, index, old string, new string

        self.list[index] = new_string
        self.ask_for_command()

    # Delete given index. Add additional properties to self.dict_steps to make redo and undo methods easier to use
    def delete(self):
        delete_index = int(input("Enter index to delete: "))
        dict_steps_size = str(len(self.dict_steps))
        string = self.list[delete_index]

        self.dict_steps[dict_steps_size] = ["delete", delete_index, string] # type, at what index to add, what to add

        del self.list[delete_index]
        self.ask_for_command()

    # Undo previous action. Add additional properties to self.dict_steps to make redo method easier to use
    def undo(self):
        if len(self.dict_steps) == 0:
            print("<CAN'T UNDO>\n")
            self.print_list()
        else:
            last_index = str(len(self.dict_steps) - 1)
            dict_redo_size = str(len(self.dict_redo))
            type, index = self.dict_steps[last_index][0], self.dict_steps[last_index][1]

            if type == "add":
                string = self.list[index]
                self.dict_redo[dict_redo_size] = ["add", index, string] # type, at what index to add, what to add
                del self.list[index]

            elif type == "edit":
                old_string, new_string = self.dict_steps[last_index][2], self.dict_steps[last_index][3]
                self.dict_redo[dict_redo_size] = ["edit", index, old_string, new_string] # type, index, what to add
                self.list[index] = old_string

            elif type == "delete":
                string = self.dict_steps[last_index][2]
                self.dict_redo[dict_redo_size] = ["delete", index]  # type, what index to delete
                self.list.insert(index, string)

            del self.dict_steps[last_index]

        self.ask_for_command()

    # Redo previous action. Add additional properties to self.dict_steps to make undo method easier to use
    def redo(self):
        if len(self.dict_redo) == 0:
            print("<CAN'T REDO>\n")
        else:
            latest_action_index = str(len(self.dict_redo) - 1)
            type = self.dict_redo[latest_action_index][0]
            dict_size = str(len(self.dict_steps))

            if type == "add":
                index, string = self.dict_redo[latest_action_index][1], self.dict_redo[latest_action_index][2]
                self.dict_steps[dict_size] = ["add", index, string] # type, what index to remove, what to remove
                self.list.insert(index, string)

            elif type == "edit":
                index = self.dict_redo[latest_action_index][1]
                old_string, new_string = self.dict_redo[latest_action_index][2], self.dict_redo[latest_action_index][3]
                self.dict_steps[dict_size] = ["edit", index, old_string, new_string] # type, index, what to add
                self.list[index] = new_string

            elif type == "delete":
                index = self.dict_redo[latest_action_index][1]
                string = self.list[index]
                self.dict_steps[dict_size] = ["delete", index, string] # type, what index to add, what to add
                del self.list[index]

            del self.dict_redo[latest_action_index]

        self.ask_for_command()

    def print_list(self):
        for string in self.list:
            print(string)
        print()

EditText()
